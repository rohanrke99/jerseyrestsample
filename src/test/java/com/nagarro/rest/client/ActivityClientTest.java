package com.nagarro.rest.client;

import static org.junit.Assert.*;

import com.nagarro.rest.model.Activity;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;


public class ActivityClientTest {

    @Test
    public void getActivity() {

        ActivityClient client = new ActivityClient();

        Activity activity = client.getActivity("1234");

        System.out.println(activity);

        Assert.assertNotNull(activity);
    }


    @Test
    public void testGetList(){
        ActivityClient client = new ActivityClient();

        List<Activity> list = client.get();

        System.out.println(list.toString());

        Assert.assertNotNull(list);
    }

}