package com.nagarro.rest;

import com.nagarro.rest.model.Activity;
import com.nagarro.rest.model.User;
import com.nagarro.rest.repository.ActivityRepository;
import com.nagarro.rest.repository.ActivityResourceStub;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.util.List;

@Path("activities")
public class ActivityResource {


    private ActivityRepository activityRepository = new ActivityResourceStub();


    @POST
    @Path("activity")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
    public Activity createActivityParams(MultivaluedMap<String,String> formParams){

        System.out.println(formParams.getFirst("description"));
        System.out.println(formParams.getFirst("duration"));

        Activity activity = new Activity();
        activity.setDescription(formParams.getFirst("description"));
        activity.setDuration(Integer.parseInt(formParams.getFirst("duration")));

        return activity;

    }

    @POST
    @Path("activity")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
    public Activity createActivity(Activity activity){

        System.out.println(activity.getDescription());
        System.out.println(activity.getDuration());

        return activityRepository.createActivity(activity);

    }



    @GET
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    public List<Activity> getAllActivities(){
        return activityRepository.listAllActivities();
    }




    @GET
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    @Path("{activityId}")
    public Activity getActivity(@PathParam("activityId") String activityId){

        System.out.println("Getting Activity Id  : " + activityId);
        return activityRepository.findActivity(activityId);
    }




    @GET
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    @Path("{activityId}/user")
    public User getActivityUser(@PathParam("activityId") String activityId){
        return activityRepository.findActivity(activityId).getUser();
    }



}
