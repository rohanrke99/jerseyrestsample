package com.nagarro.rest.client;

import com.nagarro.rest.model.Activity;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.util.List;

public class ActivityClient {

    private Client client;

    public ActivityClient(){
        client = ClientBuilder.newClient();
    }

    public Activity getActivity(String id){
        WebTarget target = client.target("http://localhost:8080/webapi/");
       // Activity response  = target.path("activities/" + id).request().get(Activity.class);
       Activity response  = target.path("activities/" + id).request(MediaType.APPLICATION_JSON).get(Activity.class);

       return response;
    }


    public List<Activity> get(){

        WebTarget target = client.target("http://localhost:8080/webapi/");
        List<Activity> list  = target.path("activities").request(MediaType.APPLICATION_JSON).get(List.class);

        return list;
    }
}
