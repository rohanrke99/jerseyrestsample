package com.nagarro.rest.model;

import lombok.Data;
import lombok.ToString;

import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement
@ToString
public class Activity {


    private String activityId;
    private String description;
    private int duration;
    private User user;
}
