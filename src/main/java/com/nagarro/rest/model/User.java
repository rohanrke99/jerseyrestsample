package com.nagarro.rest.model;

import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement
public class User {

    private String id;
    private String name;
}
