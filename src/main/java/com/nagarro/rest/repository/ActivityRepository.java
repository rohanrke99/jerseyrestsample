package com.nagarro.rest.repository;

import com.nagarro.rest.model.Activity;

import java.util.List;

public interface ActivityRepository {

    List<Activity> listAllActivities();

    Activity findActivity(String activityId);

    Activity createActivity(Activity activity);
}
