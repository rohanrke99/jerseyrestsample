package com.nagarro.rest.repository;

import com.nagarro.rest.model.Activity;
import com.nagarro.rest.model.User;
import org.omg.PortableInterceptor.ACTIVE;

import java.util.ArrayList;
import java.util.List;

public class ActivityResourceStub implements ActivityRepository{


    @Override
    public List<Activity> listAllActivities(){

        List<Activity> list = new ArrayList<>();

        Activity one = new Activity();
        one.setDescription("Swimming");
        one.setDuration(55);

        list.add(one);



        Activity two = new Activity();
        two.setDescription("Cycling");
        two.setDuration(120);
        list.add(two);

        return list;


    }


    @Override
    public Activity findActivity(String activityId) {

        Activity activity = new Activity();
        activity.setActivityId("234");
        activity.setDuration(120);
        activity.setDescription("Swimming");

        User user = new User();
        user.setId("578");
        user.setName("Rohan");

        activity.setUser(user);
        return activity;
    }

    @Override
    public Activity createActivity(Activity activity) {

        User user = new User();
        user.setId("578");
        user.setName("Rohan");
        activity.setUser(user);

        return activity;
    }
}
